var clientesObtenidos;

function getClientes(){

  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    if (this.readyState == 4 && this.status ==200){
      //console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes();
       }
  }

  request.open("GET",url,true)
  request.send();
}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  var divTabla= document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");


  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  //alert(JSONClientes.value[0]);
  for (var i=0; i < JSONClientes.value.length; i++){
    //console.log(JSONClientes.value[i].ProductNAme);
    var nuevaFila = document.createElement("tr");

    var columnNombre = document.createElement("td");
    columnNombre.innerText = JSONClientes.value[i].ContactName;

    var columnCiudad = document.createElement("td");
    columnCiudad.innerText = JSONClientes.value[i].City;

    var columnBandera = document.createElement("td");
    var imgBandera = document.createElement("img");

    if(JSONClientes.value[i].Country == "UK"){
       imgBandera.src = rutaBandera + "United-Kingdom.png";
    } else {
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }

    imgBandera.style.width = "70px";
    imgBandera.style.width = "40px";
    columnBandera.appendChild(imgBandera);
    //columnBandera.innerText = JSONClientes.value[i].Country;

    nuevaFila.appendChild(columnNombre);
    nuevaFila.appendChild(columnCiudad);
    nuevaFila.appendChild(columnBandera);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
